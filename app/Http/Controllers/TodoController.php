<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TodoController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function showAllTodos()
    {
    	return view('todo.show-all');
    }
}
