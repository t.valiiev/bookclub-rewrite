<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Page as BasePage;
use Tests\DuskTestCase;

abstract class Page extends BasePage
{
    /**
     * Get the global element shortcuts for the site.
     *
     * @return array
     */
    public static function siteElements()
    {
        return [
          // '@element' => '#selector',
        ];
    }

    public static function baseUrl(): string
    {
        return DuskTestCase::getBaseUrl();
    }

    public static function getDefaultWaitTime()
    {
        return env('DEFAULT_WAIT_TIME', 5);
    }
}
