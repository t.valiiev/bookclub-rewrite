<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\AboutPage;
use Tests\DuskTestCase;

class RoutingToAboutTest extends DuskTestCase
{

    /**
     * @throws \Throwable
     */
	public function testRoutingToAboutPage()
	{
		$this->browse(function (Browser $browser) {
			$browser->visit(new AboutPage);
            $browser->screenshot('about_page');
		});
	}
}
