## Installing

### Requirements

+ [docker](https://www.docker.com/get-docker)
+ [docker-compose](https://docs.docker.com/compose/install)

Just pls run it one-liner:

`git clone git@gitlab.com:ToKaTpoHb/bookclub-rewrite.git && cd bookclub-rewrite && make init`

## Testing

To run tests:

`make test`

## Provided setup

Application is listening on port 8080, phpmyadmin is listening on port 8081