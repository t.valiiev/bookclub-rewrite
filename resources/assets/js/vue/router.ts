import Vue from "vue";
import Router from "vue-router";

const Home = () => import(/* webpackChunkName: "js/r/home" */ './views/Home.vue');
const About = () => import(/* webpackChunkName: "js/r/about" */ './views/About.vue');
export default new Router({
    mode: "history",
    base: '/',
    routes: [
        {path: "/", name: "home", component: Home},
        {path: "/about", name: "about", component: About}
    ]
});

Vue.use(Router);